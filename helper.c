//
// Created by hakami on 11/27/16.
//
#include <stdlib.h>
#include <time.h>
#include "helper.h"

mpz_ptr mpz_new(void) {
    mpz_ptr result = malloc(sizeof(mpz_t));
    mpz_init(result);
    return result;
}

struct point_t point_new(void) {
    return (struct point_t) {mpz_new(), mpz_new()};
}

#define MAX_STACK_SIZE 500

static mpz_ptr stack[MAX_STACK_SIZE];
static mpz_ptr *top;

static mpz_ptr num_place;
static struct point_t point_place;

static gmp_randstate_t r_state;

void init_stack(void) {
    for (int i = 0; i < MAX_STACK_SIZE; i++) {
        stack[i] = malloc(sizeof(mpz_t));
        mpz_init(stack[i]);
    }

    top = stack;

    num_place = mpz_new();
    point_place = point_new();

    gmp_randinit_default(r_state);
    gmp_randseed_ui(r_state, (unsigned long) time(0));
}

mpz_ptr ret(mpz_ptr val) {
    top = stack;
    return val;
}

mpz_ptr randomm(mpz_srcptr max) {
    mpz_ptr result = *top++;
    mpz_urandomm(result, r_state, max);
    return result;
}

mpz_ptr mult(mpz_srcptr a, mpz_srcptr b) {
    mpz_ptr result = *top++;
    mpz_mul(result, a, b);
    return result;
}

mpz_ptr multm(mpz_srcptr a, mpz_srcptr b, mpz_srcptr n) {
    return mod(mult(a, b), n);
}

mpz_ptr mult_d(mpz_srcptr a, long long b) {
    mpz_ptr bz = *top++;
    mpz_set_d(bz, b);
    return mult(a, bz);
}

mpz_ptr mult_dm(mpz_srcptr a, long long b, mpz_srcptr n) {
    return mod(mult_d(a, b), n);
}

mpz_ptr sum(mpz_srcptr a, mpz_srcptr b) {
    mpz_ptr result = *top++;
    mpz_add(result, a, b);
    return result;
}

mpz_ptr summ(mpz_srcptr a, mpz_srcptr b, mpz_srcptr n) {
    return mod(sum(a, b), n);
}

mpz_ptr mod(mpz_srcptr n, mpz_srcptr mod) {
    mpz_ptr result = *top++;
    mpz_mod(result, n, mod);
    return result;
}

mpz_ptr mod_d(mpz_srcptr n, long long m) {
    mpz_ptr modd = *top++;
    mpz_set_si(modd, m);
    return mod(n, modd);
}

mpz_ptr sub(mpz_srcptr a, mpz_srcptr b) {
    mpz_ptr result = *top++;
    mpz_sub(result, a, b);
    return result;
}

mpz_ptr sub_d(mpz_srcptr a, long long b) {
    mpz_ptr bz = *top++;
    mpz_set_d(bz, b);
    return sub(a, bz);
}

mpz_ptr subm(mpz_srcptr a, mpz_srcptr b, mpz_srcptr n) {
    return mod(sub(a, b), n);
}

mpz_ptr sub_dm(mpz_srcptr a, long long b, mpz_srcptr n) {
    return mod(sub_d(a, b), n);
}

mpz_ptr divide(mpz_srcptr a, mpz_srcptr b) {
    mpz_ptr result = *top++;
    mpz_div(result, a, b);
    return result;
}

mpz_ptr divide_d(mpz_srcptr a, long long b){
    mpz_ptr bb = *top++;
    mpz_set_si(bb, b);
    return divide(a, bb);
}

mpz_ptr gcd(mpz_srcptr a, mpz_srcptr b) {
    mpz_ptr result = *top++;
    mpz_gcd(result, a, b);
    return result;
}

void inverse(mpz_ptr y, mpz_srcptr p, mpz_srcptr i) {
    mpz_ptr d = *top++;
    mpz_ptr x = *top++;

    mpz_gcdext(d, x, y, p, i);

    if (mpz_cmp_si(y, 0) < 0) {
        mpz_add(y, y, p);
    }
}

void sum_points(struct point_t *result, mpz_ptr inv, struct point_t p1, struct point_t p2, mpz_srcptr n, mpz_srcptr a) {
    if (mpz_cmp(p1.x, p2.x) == 0 && mpz_cmp(p1.y, p2.y) == 0) {
        double_point(result, inv, p1, n, a);
    } else {
        sum_diff_points(result, inv, p1, p2, n, a);
    }
}

void mult_point(struct point_t *result, mpz_ptr inv, mpz_srcptr num, struct point_t p, mpz_srcptr n, mpz_srcptr a) {
    mpz_ptr num1 = num_place;
    struct point_t point = point_place;

    mpz_set(num1, num);

    mpz_set(point.x, p.x);
    mpz_set(point.y, p.y);

    mpz_set(num1, ret(sub_d(num1, 1)));

    while (mpz_cmp_si(num1, 0) > 0) {
        if (mpz_cmp_si(mod_d(num1, 2), 0) != 0) {
            sum_points(&point, inv, point, p, n, a);

            mpz_set(num1, ret(sub_d(num1, 1)));
        }

        double_point(&point, inv, point, n, a);
        mpz_set(num1, ret(divide_d(num1, 2)));
    }

    mpz_set(result->x, point.x);
    mpz_set(result->y, point.y);
}

void double_point(struct point_t *result, mpz_ptr inv, struct point_t p, mpz_srcptr n, mpz_srcptr a) {
    mpz_ptr lambda = *top++;
    mpz_ptr x_result = *top++;
    mpz_ptr y_result = *top++;

    inverse(inv, n, mult_dm(p.y, 2, n));
    mpz_set(lambda, mult(sum(a, mult(p.x, mult_d(p.x, 3))), inv)); //mod?

    mpz_set(x_result, subm(subm(multm(lambda, lambda, n), p.x, n), p.x, n));
    mpz_set(y_result, subm(multm(lambda, sub(p.x, x_result), n), p.y, n));

    mpz_set(result->x, x_result);
    mpz_set(result->y, y_result);

    ret(NULL);
}

void sum_diff_points(struct point_t *result, mpz_ptr inv, struct point_t p1, struct point_t p2, mpz_srcptr n, mpz_srcptr a) {
    mpz_ptr lambda = *top++;
    mpz_ptr x_result = *top++;
    mpz_ptr y_result = *top++;

    inverse(inv, n, subm(p2.x, p1.x, n));
    mpz_set(lambda, multm(subm(p2.y, p1.y, n), inv, n));

    mpz_set(x_result, subm(subm(multm(lambda, lambda, n), p1.x, n), p2.x, n));
    mpz_set(y_result, subm(multm(lambda, sub(p1.x, x_result), n), p1.y, n));

    mpz_set(result->x, x_result);
    mpz_set(result->y, y_result);

    ret(NULL);
}
