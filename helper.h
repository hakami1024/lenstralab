//
// Created by hakami on 11/27/16.
//

#ifndef LABLENSTRA_HELPER_H
#define LABLENSTRA_HELPER_H

#include <gmp.h>

struct point_t {
    mpz_ptr x;
    mpz_ptr y;
};

mpz_ptr mpz_new(void);
struct point_t point_new(void);

void init_stack(void);
mpz_ptr ret(mpz_ptr val);

mpz_ptr randomm(mpz_srcptr max);

mpz_ptr sum(mpz_srcptr a, mpz_srcptr b);

mpz_ptr summ(mpz_srcptr a, mpz_srcptr b, mpz_srcptr mod);

mpz_ptr mult_dm(mpz_srcptr a, long long b, mpz_srcptr mod);

mpz_ptr mult(mpz_srcptr a, mpz_srcptr b);
mpz_ptr mult_d(mpz_srcptr a, long long b);
mpz_ptr multm(mpz_srcptr a, mpz_srcptr b, mpz_srcptr mod);

mpz_ptr mod(mpz_srcptr n, mpz_srcptr mod);

mpz_ptr sub(mpz_srcptr a, mpz_srcptr b);
mpz_ptr sub_d(mpz_srcptr a, long long b);
mpz_ptr subm(mpz_srcptr a, mpz_srcptr b, mpz_srcptr mod);
mpz_ptr sub_dm(mpz_srcptr a, long long b, mpz_srcptr mod);

mpz_ptr sqrt(mpz_srcptr n);

mpz_ptr gcd(mpz_srcptr a, mpz_srcptr b);

void double_point(struct point_t *result, mpz_ptr inv, struct point_t p, mpz_srcptr n, mpz_srcptr a);
void sum_diff_points(struct point_t *result, mpz_ptr inv, struct point_t p1, struct point_t p2, mpz_srcptr n, mpz_srcptr a);
void sum_points(struct point_t *result, mpz_ptr inv, struct point_t p1, struct point_t p2, mpz_srcptr n, mpz_srcptr a);
void mult_point(struct point_t *result, mpz_ptr inv, mpz_srcptr num, struct point_t p, mpz_srcptr n, mpz_srcptr a);
void inverse(mpz_ptr y, mpz_srcptr p, mpz_srcptr i);

#endif //LABLENSTRA_HELPER_H
