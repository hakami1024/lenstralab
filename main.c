#include <stdio.h>
#include <gmp.h>
#include <stdlib.h>
#include "helper.h"

void calc_b_for_EC(mpz_t b, mpz_t x, mpz_t y, mpz_t a, mpz_t mod) { // b = y^2 - x^3 - ax
    mpz_set(b, ret(subm(subm(multm(y, y, mod), multm(multm(x, x, mod), x, mod), mod), multm(x, a, mod), mod)));
}

void calc_g(mpz_t g, mpz_t a, mpz_t b, mpz_t n) {
    mpz_gcd(g, n, ret(sum(mult_d(mult(mult(a, a), a), 4), mult_d(mult(b, b), 27))));
}

mpz_ptr run_iteration(mpz_ptr n,
                      mpz_ptr B1, mpz_ptr B2,
                      mpz_ptr a, mpz_ptr b,
                      mpz_ptr x, mpz_ptr y,
                      mpz_ptr g,
                      mpz_ptr p, mpz_ptr ppow, mpz_ptr inv,
                      struct point_t P, struct point_t P1, struct point_t P2,
                      mpz_ptr result) {

    do {
        mpz_set(x, ret(randomm(n)));
        mpz_set(y, ret(randomm(n)));
        mpz_set(a, ret(randomm(n)));

        calc_b_for_EC(b, x, y, a, n);

        calc_g(g, a, b, n);
    } while (mpz_cmp(g, n) == 0);

    printf("a = %s.\n", mpz_get_str(NULL, 10, a));
    printf("b = %s.\n", mpz_get_str(NULL, 10, b));
    printf("x = %s.\n", mpz_get_str(NULL, 10, x));
    printf("y = %s.\n", mpz_get_str(NULL, 10, y));

    if (mpz_cmp(g, n) < 0 && mpz_cmp_si(g, 1) > 0) {
        printf("The answer is %s.\n", mpz_get_str(NULL, 10, g));
        return g;
    }

    mpz_set(P1.x, x);
    mpz_set(P1.y, y);

    mpz_set_si(p, 2);

    printf("Starting stage 1.\n");

    while (mpz_cmp(p, B1) < 0) {
        mpz_set(ppow, p);

        mpz_set(P.x, P1.x);
        mpz_set(P.y, P1.y);

        while (mpz_cmp(ppow, B1) < 0) {
            mpz_mul(ppow, ppow, p);

            mult_point(&P, inv, p, P, n, a);

            mpz_set(result, ret(gcd(n, inv)));
            if (mpz_cmp_si(result, 1) > 0) {
                printf("The answer is %s\n", mpz_get_str(NULL, 10, result));
                return result;
            }

            mpz_set(result, ret(gcd(n, P.x)));
            if (mpz_cmp_si(result, 1) > 0) {
                printf("The answer is %s\n", mpz_get_str(NULL, 10, result));

                return result;
            }

            mpz_set(result, ret(gcd(n, P.y)));
            if (mpz_cmp_si(result, 1) > 0) {
                printf("The answer is %s\n", mpz_get_str(NULL, 10, result));

                return result;
            }
        }

        mpz_nextprime(p, p);
//        printf("p is %s\n", mpz_get_str(NULL, 10, p));
    }


    printf("Starting stage 2.\n");

    mpz_set(P2.x, P.x);
    mpz_set(P2.y, P.y);

    while (mpz_cmp(p, B2) < 0) {

        mpz_set(ppow, p);

        mpz_set(P.x, P2.x);
        mpz_set(P.y, P2.y);

        while (mpz_cmp(ppow, B2) < 0) {
            mpz_mul(ppow, ppow, p);

            mult_point(&P, inv, p, P, n, a);

            mpz_set(result, ret(gcd(n, inv)));
            if (mpz_cmp_si(result, 1) > 0) {
                printf("The answer is %s\n", mpz_get_str(NULL, 10, result));
                return result;
            }

            mpz_set(result, ret(gcd(n, P.x)));
            if (mpz_cmp_si(result, 1) > 0) {
                printf("The answer is %s\n", mpz_get_str(NULL, 10, result));

                return result;
            }

            mpz_set(result, ret(gcd(n, P.y)));
            if (mpz_cmp_si(result, 1) > 0) {
                printf("The answer is %s\n", mpz_get_str(NULL, 10, result));

                return result;
            }
        }
//        printf("p is %s\n", mpz_get_str(NULL, 10, p));

        mpz_nextprime(p, p);
    }

    return NULL;
}

int main() {
    init_stack();

    mpz_ptr n = mpz_new();
    printf("n = ");
    gmp_scanf("%Zd", n);
//    mpz_init_set_str(n, "455839", 10); //599*761
//    mpz_init_set_str(n, "143", 10); //11*13
//    mpz_init_set_str(n, "45173", 10); //199*227
//    mpz_init_set_str(n, "1022117", 10); //1009*1013


    mpz_ptr B1 = mpz_new();
    mpz_sqrt(B1, n);
//    mpz_set_si(B1, 10000);
    printf("B1 = %s.\n", mpz_get_str(NULL, 10, B1));

    mpz_ptr B2 = mpz_new();
    mpz_div_ui(B2, n, 2);
//    mpz_set_si(B2, 500000);
    printf("B2 = %s.\n", mpz_get_str(NULL, 10, B2));

    mpz_ptr a = mpz_new();
    mpz_ptr b = mpz_new();
    mpz_ptr x = mpz_new();
    mpz_ptr y = mpz_new();
    mpz_ptr g = mpz_new();

    mpz_ptr ppow = mpz_new();
    mpz_ptr inv = mpz_new();
    mpz_ptr p = mpz_new();

    struct point_t P = point_new();
    struct point_t P1 = point_new();
    struct point_t P2 = point_new();

    mpz_ptr result = mpz_new();

    while (run_iteration(n, B1, B2, a, b, x, y, g, p, ppow, inv, P, P1, P2, result) == NULL);

    return 0;
}